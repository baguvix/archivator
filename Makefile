CC = gcc
INC = includes
CFLAGS = -Wall -Werror -Wextra -I$(INC)
NAME = rat
SRCS = main.c			\
       arch_meta_build.c	\
       fs_manipulating.c	\
       parser.c			\
       err.c
OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $^ -o $@

%.o: %.c %.h

clean:
	rm -f $(OBJS)

fclean: clean
	rm -f $(NAME)

re: fclean all
