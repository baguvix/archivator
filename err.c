#include "arch.h"

void	herror(void)
{
	if (errno == 0)
		dprintf(2, "Worked good!\n");
	else if (errno < AFTER_LAST_KNOWN_ERR)
		perror("Error");
	else
		switch (errno)
		{
			case ARCH_STRUCT_CORRUPTED:
				dprintf(2, "Something is wrong with archive content.\n");
				break;
			case NO_FILES:
				dprintf(2, "For archive creation files are requiered.\n");
				break;
			case BAD_USAGE:
				dprintf(2, "Try to open README at first.\n");
				break;
		}
}

