#include "arch.h"

void	input_parse(int ac, char **av, t_state *program_state)
{
	int	c;

	while ((c = getopt(ac, av, "cdn:")) != -1)
	{
		switch (c)
		{
			case 'n':
				program_state->ar_name = optarg;
				break;
			case 'c':
				program_state->action.c = 1;
				break;
			case 'd':
				program_state->action.d = 1;
				break;
			case ':':
				dprintf(2, "Argument for -%c is missing.\n", optopt);
			case '?':
				errno = BAD_USAGE;
				exit(-1);
				break;
		}
	}
	program_state->files = av + optind;
	if ((program_state->action.c && program_state->action.d)	||
	    (program_state->action.d && *program_state->files != NULL)	||
	    (program_state->ar_name == NULL))
		errno = BAD_USAGE;

	if (!errno && *program_state->files == NULL && program_state->action.c)
		errno = NO_FILES;
	
	if (errno)
		exit(-1);
}
