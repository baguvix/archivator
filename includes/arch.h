#ifndef	ARCH_H
# define ARCH_H

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <libgen.h>
#include <errno.h>

#define BUFF_SIZE		4096
#define AFTER_LAST_KNOWN_ERR	103

enum
{
	ARCH_STRUCT_CORRUPTED = AFTER_LAST_KNOWN_ERR,
	NO_FILES,
	BAD_USAGE
};

typedef	struct		tree_node
{
	struct tree_node	*left;
	struct tree_node	*right;
	char			*fname;
	unsigned		depth;
	unsigned		name_len;
	off_t			fsize;
	mode_t			mode;
}			t_node;

typedef struct
{
	char			*ar_name;
	char			**files;
	struct
	{
		unsigned int	c : 1;
		unsigned int	d : 1;
	}	action;
}			t_state;

int	serialize_node(t_node *n, int fd);
t_node	*deserialize_node( int fd);
t_node	*deserialize(t_node **root, unsigned d, int *count, int fd);
void	herror(void);
t_node	*create_tnode(char *name, unsigned d);
void	destroy_tnode(t_node **n);
void	kill_tree(t_node **root);
int	read_structure(t_node **root, unsigned d, char *file);
int	print_node(t_node *n, int pad);
int	tree_traverse(t_node *root, int a, int (*call_back)(t_node *, int));
int	write_file(t_node *n, int arfd);
int	read_file(t_node *n, int arfd);
int	fname_normalization(t_node *root);
void	input_parse(int ac, char **av, t_state *program_state);
#endif
