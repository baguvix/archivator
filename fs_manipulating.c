#include "arch.h"

t_node	*create_tnode(char *name, unsigned d)
{
	size_t		tmp;
	t_node		*new_node;
	struct stat	buf;

	new_node = (t_node *)malloc(sizeof(t_node));
	if (new_node == NULL)
		return (NULL);

	new_node->name_len = strlen(name);
	tmp = (new_node->name_len + 1) * sizeof(char);
	new_node->fname = (char *)malloc(tmp);
	if (new_node->fname == NULL)
		return (NULL);
	memcpy(new_node->fname, name, tmp);

	new_node->depth = d;

	if (lstat(new_node->fname, &buf) == -1)
		return (NULL);
	new_node->fsize = buf.st_size;
	new_node->mode  = buf.st_mode;
	new_node->left  = NULL;
	new_node->right = NULL;

	return (new_node);
}

void	destroy_tnode(t_node **n)
{
	free((*n)->fname);
	free(*n);
	*n = NULL;
}

void	kill_tree(t_node **root)
{
	if ((*root)->left)
		kill_tree(&(*root)->left);
	if ((*root)->right)
		kill_tree(&(*root)->right);
	printf("Deleting node: %s\n", (*root)->fname);
	destroy_tnode(root);
}

int	read_structure(t_node **root, unsigned d, char *file)
{
	t_node		*cur;
	int		f_c = 0;
					// reaching NULL file intance.
	while (*root)
		root = &(*root)->right;
	*root = create_tnode(file, d);
	cur = *root;
	if (cur == NULL)
		return (-1);
	++f_c;

	if (S_ISDIR(cur->mode))
	{
		DIR		*dirp;
		struct dirent	*dp;
		int		resp;
		char		cwd[MAXPATHLEN];	// <sys/param.h>

		if (getwd(cwd) == NULL || chdir(file) == -1 || (dirp = opendir(".")) == NULL)
			return (-1);

		errno = 0;
		while (!errno && (dp = readdir(dirp)) != NULL)
			if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
				if ((resp = read_structure(&cur->left, d + 1, dp->d_name)) != -1)
					f_c += resp;
		if (closedir(dirp) == -1 || errno || chdir(cwd) == -1)
			return (-1);
	}
	else if (!(S_ISREG(cur->mode) || S_ISLNK(cur->mode)))
	{
		--f_c;
		destroy_tnode(&cur);
		printf("This file couldn't be stored: %s\n", file);
	}
	return (f_c);
}

int	print_node(t_node *n, int pad)
{
	return (printf("%*s%s\n", pad * n->depth, "",n->fname));
}

int	tree_traverse(t_node *root, int a, int (*call_back)(t_node *, int))
{
	int	count = 0;

	if (call_back(root, a) == -1)
		exit(-1);

	if (root->left)
		count += tree_traverse(root->left, a, call_back);
	
	if (root->right)
		count += tree_traverse(root->right, a, call_back);
	
	return (count + 1);
}

int	write_file(t_node *n, int arfd)
{
	errno = 0;
	if (n->left)
	{
		char	cwd[MAXPATHLEN];
		
		if (getwd(cwd) != NULL && chdir(n->fname) == 0)
		{
			if (write_file(n->left, arfd) == -1)
				return (-1);
			chdir(cwd);
		}
	}

	if (errno)
		return (-1);

	if (n->right)
		if (write_file(n->right, arfd))
			return (-1);
	if (!S_ISDIR(n->mode))
	{
		ssize_t	nbytes;
		int	fd;
		char	buf[BUFF_SIZE];

		if ((fd = open(n->fname, O_RDONLY)) == -1)
			return (-1);
		
		while ((nbytes = read(fd, buf, BUFF_SIZE)))
			if (write(arfd, buf, nbytes) < 0)
				break;

		close(fd);
	}
	return (errno ? -1 : 0);
}

int	read_file(t_node *n, int arfd)
{
	errno = 0;
	if (n->left)
	{
		if ((mkdir(n->fname, n->mode) + chdir(n->fname)) == 0)
		{
			if (read_file(n->left, arfd) == -1)
				return (-1);
			chdir("..");
		}
	}

	if (errno)
		return (-1);

	if (n->right)
		if (read_file(n->right, arfd))
			return (-1);
	if (!S_ISDIR(n->mode))
	{
		ssize_t	nbytes;
		int	fd;
		char	buf[BUFF_SIZE];

		if ((fd = creat(n->fname, n->mode)) == -1)
			return (-1);

		while (n->fsize)
		{
			nbytes = (n->fsize < BUFF_SIZE ? n->fsize : BUFF_SIZE);
			if (read(arfd, buf, nbytes) < 0 || write(fd, buf, nbytes) < 0)
				break;
			n->fsize -= nbytes;
		}

		close(fd);
	}
	return (errno ? -1 : 0);
}

int	fname_normalization(t_node *root)
{
	if (root->right)
		fname_normalization(root->right);

	char	*name = basename(root->fname);		// <libgen.h>
	memcpy(root->fname, name, strlen(name) + 1);
	return (0);
}
