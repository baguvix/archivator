#include "arch.h"

int	serialize_node(t_node *n, int fd)
{
	unsigned	len;
	char		*buf;

	len = n->name_len + sizeof(n->name_len) + sizeof(n->depth);
	len += sizeof(n->fsize) + sizeof(n->mode);
	
	buf = (char *)malloc(len + sizeof(len));
	if (buf == NULL)
		return (-1);
	// length of the meta portion.
	memcpy(buf, &len, sizeof(len));
	buf += sizeof(len);

	// characters in file name.
	memcpy(buf, &n->name_len, sizeof(n->name_len));
	buf += sizeof(n->name_len);
	
	// file name characters.
	memcpy(buf, n->fname, n->name_len);
	buf += n->name_len;
	
	// file depth info.
	memcpy(buf, &n->depth, sizeof(n->depth));
	buf += sizeof(n->depth);

	// file size info.
	memcpy(buf, &n->fsize, sizeof(n->fsize));
	buf += sizeof(n->fsize);

	// file protection mode.
	memcpy(buf, &n->mode, sizeof(n->mode));
	buf += sizeof(n->mode);

	/* /\ Could be extend by any additional meta. /\ */
	
	errno = 0;
	buf -= (len + sizeof(len));
	write(fd, buf, len + sizeof(len));
	free(buf);
	if (errno)
		return (-1);
	return (0);
}

t_node	*deserialize_node( int fd)
{
	char		*buf;
	t_node		*n;
	unsigned	len;
	
	if (read(fd, &len, sizeof(len)) == -1)
		exit(-1);
	buf = (char *)malloc(len);

	n = (t_node *)malloc(sizeof(t_node));
	if (n == NULL || buf == NULL)
		return (NULL);
	
	if (read(fd, buf, len) == -1)
		exit(-1);
	n->name_len = *((unsigned *)(buf));
	buf += sizeof(unsigned);
	
	n->fname = (char *)malloc((n->name_len + 1) * sizeof(char));
	if (n->fname == NULL)
		return (NULL);
	memcpy(n->fname, buf, n->name_len);
	n->fname[n->name_len] = '\0';
	buf += n->name_len;

	n->depth = *((unsigned *)buf);
	buf += sizeof(unsigned);

	n->fsize = *((off_t *)buf);
	buf += sizeof(off_t);

	n->mode = *((mode_t *)buf);
	buf += sizeof(mode_t);

	n->left = NULL;
	n->right = NULL;
	buf -= len;
	free(buf);
	return (n);
}

t_node	*deserialize(t_node **root, unsigned d, int *count, int fd)
{
	t_node	*n = NULL;


	while (*count || n)
	{
		if (n == NULL) {
			n = deserialize_node(fd);
			*count -= 1;
		}
		if (n->depth < d)
			return (n);
		if (*root == NULL)
			*root = n;
		if (S_ISDIR(n->mode)) {
			if ((n = deserialize(&(*root)->left, d + 1, count, fd)) != NULL)
				root = &(*root)->right;
			continue;
		}
		if ((S_ISREG(n->mode) || S_ISLNK(n->mode)))
			n = deserialize(&(*root)->right, d, count, fd);
		else
		{
			errno = ARCH_STRUCT_CORRUPTED;
			exit(-1);
		}
	}
	return (n);
}
