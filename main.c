#include "arch.h"

int	main(int argc, char **argv)
{
	t_node		*root;
	int		arfd;
	int		files_count = 0, tmp;
	t_state		meta;

	if (atexit(herror))
	{
		dprintf(2, "Errors handler function registration failed.\n");
		exit(-1);
	}
	input_parse(argc, argv, &meta);

	if (meta.action.c) {
		arfd = creat(meta.ar_name, S_IRWXU | S_IRGRP);
		if (arfd == -1)
			exit(-1);

		while (*meta.files)
			if ((tmp = read_structure(&root, 0, *meta.files++)) == -1)
			{	
				kill_tree(&root);
				exit(-1);
			}
			else
				files_count += tmp;				// we can count number of entries by read_str func and put it in file at first,	
		printf("Files were found: %d\n", files_count);			// or get this number from tree_traverse, seek forward arch_file position and than 
										// rewind the file and insert that number. I'll choose the first one.
	
		if (write(arfd, &files_count, sizeof(files_count)) == -1)	// prints structure of the future arch for us. Could be disabled.
		{
			kill_tree(&root);
			exit(-1);
		}
		tree_traverse(root, 4, print_node);
		tree_traverse(root, arfd, serialize_node);
		/* write all files */
	
		if (write_file(root, arfd) == -1)
			exit(-1);

		/* write end */
		kill_tree(&root);
		close(arfd);
		//exit(0);
	} else if (meta.action.d) {
		// decompose pipeline
		arfd = open(meta.ar_name, O_RDONLY);
		if (arfd == -1)
			exit(-1);

		if (read(arfd, &files_count, sizeof(files_count)) == -1)
			exit(-1);
		deserialize(&root, 0, &files_count, arfd);
		/* read all files */

		fname_normalization(root);
		tree_traverse(root, 4, print_node);
		if  (read_file(root, arfd))
			exit(-1);

		/* read end */
		kill_tree(&root);
		close(arfd);
	} else {
		errno = BAD_USAGE;
		exit (-1);
	}
	exit(0);
}
